package com.codingcrayons.aflab.util;

import com.codingcrayons.aflab.model.Country;

import javax.el.ValueExpression;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.inject.Inject;
import javax.inject.Named;

@Named
public class GeneralConverter implements Converter {

	private static final String NULL_ENTITY = "null";
	@Inject
	private EntityLists entityLists;

	@Override
	public Object getAsObject(FacesContext context, UIComponent component, String value) {
		Class<?> entityClass = getEntityClass(context, component);
		Object obj = null;
		if (!NULL_ENTITY.equals(value) && !value.isEmpty() && entityClass != null) {
			try {
				obj = entityLists.getCountries().get(Integer.parseInt(value));
			} catch (Exception e) {
				throw new ConverterException(e);
			}
		}
		return obj;
	}

	@Override
	public String getAsString(FacesContext context, UIComponent component, Object obj) {
		if (obj == null) {
			return NULL_ENTITY;
		}
		return obj instanceof Country ? ((Country)obj).getId().toString() : obj.toString();
	}

	protected Class<?> getEntityClass(FacesContext context, UIComponent component) {
		Class<?> clazz = component.getValueExpression("value").getType(context.getELContext());
		if ("Object".equals(clazz.getSimpleName())) {
			// was not successful
			ValueExpression ve = component.getValueExpression("value");
			Object instanceObject = ve.getValue(context.getELContext());
			if (instanceObject == null) {
				return null;
			} else {
				clazz = instanceObject.getClass();
			}
		}
		return clazz;
	}
}
