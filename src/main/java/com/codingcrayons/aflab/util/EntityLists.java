package com.codingcrayons.aflab.util;

import com.codingcrayons.aflab.model.Country;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Named;

@Named("entities")
@RequestScoped
public class EntityLists implements Serializable {

	private static final long serialVersionUID = 8613984312925311971L;

	public List<Country> getCountries() {
		List<Country> countries = new ArrayList<Country>();
		countries.add(new Country(0, "Czech Republic"));
		countries.add(new Country(1, "Germany"));
		countries.add(new Country(2, "France"));
		return countries;
	}
}
