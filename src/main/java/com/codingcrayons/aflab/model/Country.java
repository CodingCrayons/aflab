package com.codingcrayons.aflab.model;

import com.codingcrayons.aspectfaces.annotations.UiOrder;

import javax.validation.constraints.NotNull;

public class Country {

	private static final long serialVersionUID = 28201870652640940L;
	private String name;
	private Integer id;

	public Country(Integer id, String name) {
		this.name = name;
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	@UiOrder(1)
	@NotNull
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (o == null || getClass() != o.getClass()) {
			return false;
		}

		Country country = (Country) o;

		return id != null && id.equals(country.id);
	}

	@Override
	public int hashCode() {
		int result = name != null ? name.hashCode() : 0;
		result = 31 * result + (id != null ? id.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return name;
	}
}
